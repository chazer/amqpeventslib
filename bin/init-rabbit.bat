@echo on
cmd /c rabbitmqctl add_vhost test_amqpeventslib
cmd /c rabbitmqctl add_user amqpeventslib amqpeventslib
cmd /c rabbitmqctl set_permissions -p test_amqpeventslib amqpeventslib ".*" ".*" ".*"
