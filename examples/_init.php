<?php

use AmqpEventsLib\Adapters\PhpAmqpAdapter;
use AmqpEventsLib\AmqpEventsLib;
use PhpAmqpLib\Connection\AMQPStreamConnection;

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../AmqpEventsLib/Tests/config.php';

$lib = new AmqpEventsLib();
$lib->setAdapter(new PhpAmqpAdapter(new AMQPStreamConnection(HOST, PORT, USER, PASS, VHOST)));
$lib->exchange = 'events';
$lib->defaultSenderName = 'system';

// Init exchange if not exist
$lib->initExchange();

return $lib;
