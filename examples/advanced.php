<?php

use AmqpEventsLib\AmqpEventsLib;
use AmqpEventsLib\Events\Internal\MessageEvent;
use AmqpEventsLib\interfaces\IConsumer;
use AmqpEventsLib\Interfaces\IMessage;

/** @var AmqpEventsLib $lib */
$lib = require(__DIR__ . '/_init.php');

// Monitor listener
$lib->addListener(AmqpEventsLib::ON_BEFORE_MESSAGE,
    function (MessageEvent $event)
    use ($lib) {
        $message = $event->message;
        echo sprintf(" %8s> %s (for %s)", $message->sender, json_encode($message), $event->consumer->getName() ?: '*') . PHP_EOL;
    });

/** @var callable $hListenerA */
$hListenerA =
    function (IMessage $message, IConsumer $consumer)
    use ($lib, &$hListenerA) {
        $lib->sendMessage('hello', 'Hello from Mike', ['sender' => 'Mike']);
        $lib->removeMessageListener($hListenerA);
    };
$lib->addMessageListener($hListenerA, ['intro']);

/** @var callable $hListenerB */
$hListenerB =
    function (IMessage $message, IConsumer $consumer)
    use ($lib, &$hListenerB) {
        $lib->sendMessage('hello', 'Hello from Sam', ['sender' => 'Sam']);
        $lib->removeMessageListener($hListenerB);
    };
$lib->addMessageListener($hListenerB, ['intro']);

$lib->addConsumer($consumer = $lib->createConsumer('test'));

/** @var callable $hListenerC */
$hListenerC =
    function (IMessage $message, IConsumer $consumer) use ($lib, &$hListenerC) {
        $lib->sendMessage('hello', 'Hello from John', ['sender' => 'John']);
        $consumer->removeMessageListener($hListenerC);
    };
$consumer->addMessageListener($hListenerC, 'intro');

/** @var callable $hListenerC */
$hListenerD =
    function (IMessage $message, IConsumer $consumer)
    use ($lib, &$hListenerD) {
        echo sprintf('Drop "%s" from <%s>', $message->data, $consumer->getName()) . PHP_EOL;
        $message->dropped = true;
    };
$consumer->addMessageListener($hListenerD);

echo sprintf('Attached %d listeners', $lib->getListenersCount()) . PHP_EOL;

$lib->sendMessage('intro', 'Say');

$lib->run(1, function () use ($lib, &$hListenerC, &$hListenerD) {
    $lib->stop();
});

// Destroy
unset($lib);
