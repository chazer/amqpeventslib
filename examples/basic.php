<?php

use AmqpEventsLib\AmqpEventsLib;
use AmqpEventsLib\Events\Internal\MessageEvent;
use AmqpEventsLib\Interfaces\IMessage;

/** @var AmqpEventsLib $lib */
$lib = require(__DIR__ . '/_init.php');

// Monitor listener
$lib->addListener(AmqpEventsLib::ON_MESSAGE,
    function (MessageEvent $event) use ($lib) {
        $message = $event->message;
        echo sprintf(" %8s> %s %s", $message->sender, $message->event, $message->data) . PHP_EOL;
    });

$lib->addMessageListener(
    function (IMessage $message) use ($lib) {
        $lib->sendMessage('pong', $message->data, ['sender' => 'client']);
    }, 'ping');

$counter = 10;
$lib->run(1,
    function () use ($lib, &$counter) {
        if ($counter-- <= 0) {
            $lib->stop();
            return;
        }
        $lib->sendMessage('ping', $counter);
    });

// Destroy
unset($lib);
