<?php

use AmqpEventsLib\AmqpEventsLib;
use AmqpEventsLib\Consumer;
use AmqpEventsLib\Events\Internal\MessageEvent;
use AmqpEventsLib\Interfaces\IMessage;

/** @var AmqpEventsLib $lib */
$lib = require(__DIR__ . '/_init.php');

$next = 1;

// Monitor listener
$lib->getConsumer()->getDispatcher()->addListener(Consumer::ON_BEFORE_MESSAGE,
    function (MessageEvent $event) use (&$next) {
        if ($event->message->event === 'order') {
            $data = $event->message->data;
            assert($next <= $data, 'Wrong order');
            if ($next !== $data) {
                echo 'next: ' . $next . "\r";
                $event->stopPropagation(); // skip message
            } else {
                $next++; // wait next number
            }
        }
    });
$lib->addMessageListener(
    function (IMessage $message)
    use ($lib) {
        echo sprintf(" %8s> %s", $message->sender, json_encode($message)) . PHP_EOL;
    }, ['order']);


for ($i = 100; $i > 0; $i--) {
    $lib->sendMessage('order', $i);
}

$counter = 2;
$lib->run(1,
    function () use ($lib, &$counter) {
        if ($counter-- <= 0) {
            $lib->stop();
            return;
        }
    });

// Destroy
unset($lib);
