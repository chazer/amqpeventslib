<?php
/**
 * TimeoutException.php
 *
 * @author: chazer
 * @created: 07.09.15 13:18
 */

namespace AmqpEventsLib\Exceptions;

use Exception;

class TimeoutException extends Exception
{
}
