<?php
/**
 * .php
 *
 * @author: chazer
 * @created: 15.08.15 2:53
 */

namespace AmqpEventsLib;

use AmqpEventsLib\Interfaces\IMessage;

class Message implements IMessage
{
    public $event;
    public $sender;
    public $dropped;
    public $data;
}
