<?php
/**
 * MessageEvent.php
 *
 * @author: chazer
 * @created: 13.08.15 2:14
 */

namespace AmqpEventsLib\Events\Internal;

use AmqpEventsLib\Interfaces\IConsumer;
use AmqpEventsLib\Interfaces\IMessage;
use Symfony\Component\EventDispatcher\Event;

class MessageEvent extends Event
{
    /**
     * @var IMessage
     */
    public $message;

    /**
     * @var IConsumer
     */
    public $consumer;

    /**
     * Constructor
     *
     * @param IMessage $message
     * @param string $consumer
     */
    public function __construct(IMessage $message = null, IConsumer $consumer = null)
    {
        $this->message = $message;
        $this->consumer = $consumer;
    }
}
