<?php
/**
 * ConsumerEvent.php
 *
 * @author: chazer
 * @created: 17.08.15 14:27
 */

namespace AmqpEventsLib\Events\Internal;

use AmqpEventsLib\Consumer;
use AmqpEventsLib\interfaces\IConsumer;
use Symfony\Component\EventDispatcher\Event;

class ConsumerEvent extends Event
{
    /**
     * @var Consumer
     */
    public $consumer;

    /**
     * @var string
     */
    public $eventName;

    /**
     * @var callable
     */
    public $listener;

    /**
     * @var int
     */
    public $priority;

    /**
     * Constructor
     *
     * @param IConsumer $consumer
     * @param string $eventName
     * @param callback $listener
     * @param int $priority
     */
    function __construct(IConsumer $consumer, $eventName, $listener, $priority = 0)
    {
        $this->consumer = $consumer;
        $this->eventName = $eventName;
        $this->listener = $listener;
        $this->priority = $priority;
    }
}
