<?php
/**
 * ConsumerSubscriber.php
 *
 * @author: chazer
 * @created: 17.08.15 1:28
 */

namespace AmqpEventsLib;

use AmqpEventsLib\Events\Internal\MessageEvent;
use AmqpEventsLib\Interfaces\IConsumer;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ConsumerSubscriber implements EventSubscriberInterface
{
    /**
     * @var callable
     */
    private $handler;

    /**
     * @var string[]
     */
    private $events = [];

    /**
     * @param callable $handler
     * @param array $events list of supported events
     */
    function __construct($handler, $events = [])
    {
        $this->handler = $handler;
        $this->events = $events;
    }

    public static function getSubscribedEvents()
    {
        return [
            IConsumer::ON_MESSAGE => 'onMessage',
        ];
    }

    /**
     * @param MessageEvent $event
     */
    public function onMessage(MessageEvent $event)
    {
        if (count($this->events) > 0 && in_array($event->message->event, $this->events)) {
            call_user_func_array($this->handler, [$event->message, $event->consumer]);
        }
    }
}
