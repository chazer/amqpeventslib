<?php
/**
 * Consumer.php
 *
 * @author: chazer
 * @created: 18.08.15 1:10
 */

namespace AmqpEventsLib;

use AmqpEventsLib\Events\Internal\ConsumerEvent;
use AmqpEventsLib\Events\Internal\MessageEvent;
use AmqpEventsLib\Interfaces\IConsumer;
use AmqpEventsLib\Interfaces\IMessage;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class Consumer implements EventDispatcherInterface, IConsumer
{
    const ON_LISTENER_DETACH = 'AmqpEventsLib\Consumer.onDetachListener';
    const ON_LISTENER_ATTACH = 'AmqpEventsLib\Consumer.onAttachListener';

    private $name;
    private $object;
    private $queue;
    private $dispatcher;
    private $wrapDispatcher;

    /**
     * @var ObjectsList
     */
    private $listenersList;

    public function __construct($name = null)
    {
        $this->name = $name;
    }

    /**
     * @param EventDispatcherInterface $dispatcher
     */
    public function setRealDispatcher($dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    /**
     * @return EventDispatcherInterface
     */
    public function getRealDispatcher()
    {
        return $this->dispatcher ?: $this->dispatcher = new EventDispatcher();
    }

    private function getSubscriberWrapperDispatcher()
    {
        if (isset($this->wrapDispatcher)) {
            return $this->wrapDispatcher;
        }

        $dispatcher = new ConsumerEventDispatcher($this);

        $dispatcher->addListener(ConsumerEventDispatcher::ON_LISTENER_REMOVE,
            function (ConsumerEvent $event) {
                $this->removeListener($event->eventName, $event->listener);
            }
        );
        $dispatcher->addListener(ConsumerEventDispatcher::ON_LISTENER_ADD,
            function (ConsumerEvent $event) {
                $this->addListener($event->eventName, $event->listener, $event->priority);
            }
        );

        return $this->wrapDispatcher = $dispatcher;
    }

    protected function getListenersList()
    {
        return $this->listenersList ?: $this->listenersList = new ObjectsList();
    }

    /**
     * @param IMessage $message
     * @return bool
     */
    protected function onBeforeMessage(IMessage $message)
    {
        $event = new MessageEvent($message, $this);
        $this->getDispatcher()->dispatch(IConsumer::ON_BEFORE_MESSAGE, $event, $this->getName());
        return !$event->isPropagationStopped();
    }

    protected function onMessage(IMessage $message)
    {
        $this->getDispatcher()->dispatch(IConsumer::ON_MESSAGE, new MessageEvent($message, $this));
    }

    protected function onAfterMessage(IMessage $message)
    {
        $this->getDispatcher()->dispatch(IConsumer::ON_AFTER_MESSAGE, new MessageEvent($message, $this));
    }

    /*
     * IConsumer interface implementation
     * ----------------------------------
     */

    public function getName()
    {
        return $this->name;
    }

    public function setConsumerObject($object)
    {
        $this->object = $object;
    }

    public function getConsumerObject()
    {
        return $this->object;
    }

    public function setQueueObject($object)
    {
        $this->queue = $object;
    }

    public function getQueueObject()
    {
        return $this->queue;
    }

    /**
     * @return EventDispatcherInterface
     */
    public function getDispatcher()
    {
        return $this;
    }

    public function processMessage(IMessage $message)
    {
        if ($this->onBeforeMessage($message)) {
            $this->onMessage($message);
            $this->onAfterMessage($message);
            return true;
        } else {
            return false;
        }
    }

    public function addMessageListener($callback, $events = null)
    {
        $filter = $events ? (array) $events : [];
        $key = $callback;
        $index = $this->getListenersList()->indexOf($key);
        if ($index >= 0) {
            $data = $this->getListenersList()->getItemData($index);
            $data->events = array_merge($data->events, $filter);
        } else {
            $data = new \stdClass();
            $data->listener =
                function (MessageEvent $event)
                use ($callback, &$data) {
                    if (!$data->events || in_array($event->message->event, $data->events)) {
                        call_user_func_array($callback, [$event->message, $this]);
                    }
                };
            $data->events = $filter;
            $this->getListenersList()->addItem($key, $data);
            $this->addListener(IConsumer::ON_MESSAGE, $data->listener);
        }
    }

    public function removeMessageListener($callback, $events = null)
    {
        $key = $callback;
        $index = $this->getListenersList()->indexOf($key);
        $data = $this->getListenersList()->getItemData($index);
        if ($events !== null) {
            $filter = $events ? (array) $events : [];
            $data->events = array_diff($data->events, $filter);
            if (!empty($data->events)) {
                return;
            }
        }
        $this->getListenersList()->removeItem($index);
        $this->removeListener(IConsumer::ON_MESSAGE, $data->listener);
    }

    public function getListenersCount()
    {
        return count($this->getListeners());
    }

    /*
     * EventDispatcherInterface implementation
     * ----------------------------------
     */

    public function dispatch($eventName, Event $event = null)
    {
        return $this->getRealDispatcher()->dispatch($eventName, $event);
    }

    public function addSubscriber(EventSubscriberInterface $subscriber)
    {
        $this->getSubscriberWrapperDispatcher()->addSubscriber($subscriber);
    }

    public function removeSubscriber(EventSubscriberInterface $subscriber)
    {
        $this->getSubscriberWrapperDispatcher()->removeSubscriber($subscriber);
    }

    public function getListeners($eventName = null)
    {
        return $this->getRealDispatcher()->getListeners($eventName);
    }

    public function hasListeners($eventName = null)
    {
        return $this->getRealDispatcher()->hasListeners($eventName);
    }

    public function addListener($eventName, $listener, $priority = 0)
    {
        $event = new ConsumerEvent($this, $eventName, $listener, $priority);
        $this->dispatch(self::ON_LISTENER_ATTACH, $event);
        $this->getRealDispatcher()->addListener($eventName, $listener, $priority);
    }

    public function removeListener($eventName, $listener)
    {
        $event = new ConsumerEvent($this, $eventName, $listener);
        $this->dispatch(self::ON_LISTENER_DETACH, $event);
        $this->getRealDispatcher()->removeListener($eventName, $listener);
    }
}
