<?php
/**
 * ConsumerEventDispatcher.php
 *
 * @author: chazer
 * @created: 18.08.15 16:11
 */

namespace AmqpEventsLib;

use AmqpEventsLib\Events\Internal\ConsumerEvent;
use AmqpEventsLib\interfaces\IConsumer;
use Symfony\Component\EventDispatcher\EventDispatcher as SymfonyEventDispatcher;

class ConsumerEventDispatcher extends SymfonyEventDispatcher
{
    /** @var IConsumer */
    protected $consumer;

    const ON_LISTENER_ADD = 'EventDispatcher.addListener';
    const ON_LISTENER_REMOVE = 'EventDispatcher.removeListener';

    function __construct(IConsumer $consumer)
    {
        $this->consumer = $consumer;
    }

    public function addListener($eventName, $listener, $priority = 0)
    {
        $event = new ConsumerEvent($this->consumer, $eventName, $listener, $priority);
        $this->dispatch(self::ON_LISTENER_ADD, $event);
        parent::addListener($eventName, $listener, $priority);
    }

    public function removeListener($eventName, $listener)
    {
        $event = new ConsumerEvent($this->consumer, $eventName, $listener);
        $this->dispatch(self::ON_LISTENER_REMOVE, $event);
        parent::removeListener($eventName, $listener);
    }
}
