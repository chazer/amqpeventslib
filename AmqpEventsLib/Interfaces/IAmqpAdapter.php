<?php
/**
 * IAmqpAdapter.php
 *
 * @author: chazer
 * @created: 13.08.15 0:12
 */

namespace AmqpEventsLib\Interfaces;

use Exception;

interface IAmqpAdapter
{
    /**
     * @return mixed
     */
    public function openChannel();

    /**
     * @param mixed $channel
     */
    public function closeChannel($channel);

    /**
     * @param mixed $channel
     * @return mixed any data for queue identity
     */
    public function registerTempQueue($channel);

    /**
     * @param $channel
     * @param $name
     * @return mixed any data for queue identity
     */
    public function registerPersistentQueue($channel, $name);

    /**
     * @param mixed $channel
     * @param mixed $queue
     */
    public function removeQueue($channel, $queue);

    /**
     * @param mixed $channel
     * @param mixed $queue any data for queue identity
     * @param callable $callback
     * @param string $tag
     * @param bool $excludeLocal
     * @param bool $noAck
     * @param bool $isExclusive
     * @param bool $noWait
     * @return mixed
     */
    public function consumeQueue($channel, $queue, $callback,
                                 $tag = '', $excludeLocal = false, $noAck = false,
                                 $isExclusive = true, $noWait = false);

    /**
     * @param mixed $channel
     * @param mixed $consumer
     * @param bool $nowait
     */
    public function cancelConsume($channel, $consumer, $nowait = false);

    /**
     * @param string $name
     * @return bool
     * @throws \Exception
     */
    public function isExchangeExist($name);

    /**
     * @param mixed $channel
     * @param string $name exchange name
     * @param array $options exchange options
     * @return mixed any data for exchange identity
     */
    public function createFanoutExchange($channel, $name, $options = []);

    /**
     * @param mixed $channel
     * @param mixed $exchange
     */
    public function removeExchange($channel, $exchange);

    /**
     * @param mixed $channel
     * @param mixed $exchange any data for exchange identity
     * @param string $routingKey
     * @param mixed $message
     */
    public function sendMessage($channel, $exchange, $routingKey, $message);

    /**
     * @param mixed $channel
     * @param mixed $exchange
     * @param mixed $queue
     * @param string $routingKey
     * @throws \RuntimeException
     */
    public function bindQueue($channel, $exchange, $queue, $routingKey = null);

    /**
     * @param mixed $channel
     * @param mixed $exchange
     * @param mixed $queue
     * @throws \RuntimeException
     */
    public function unbindQueue($channel, $exchange, $queue);

    /**
     * @param mixed $channel
     * @param int $timeout
     * @throws \AmqpEventsLib\Exceptions\TimeoutException
     */
    public function waitChannel($channel, $timeout);

    /**
     * @param mixed $channel
     * @return int
     */
    public function getConsumersCount($channel);

    /**
     * @param mixed $message
     */
    public function ackMessage($message);

    /**
     * @param mixed $message
     * @param bool $requeue
     */
    public function nackMessage($message, $requeue = false);

    /**
     * @param mixed $message
     * @param bool $requeue
     */
    public function rejectMessage($message, $requeue = true);

    /**
     * @param IMessage $message
     * @return mixed
     */
    public function packMessage(IMessage $message);

    /**
     * @param mixed $message
     * @return IMessage
     */
    public function unpackMessage($message);

    /**
     * @param mixed $exchange
     * @return string
     */
    public function getExchangeType($exchange);
}
