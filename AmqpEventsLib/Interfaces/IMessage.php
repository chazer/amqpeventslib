<?php
/**
 * IMessage.php
 *
 * @author: chazer
 * @created: 15.08.15 2:36
 */

namespace AmqpEventsLib\Interfaces;

/**
 * Interface IMessage
 *
 * @property string $event
 * @property string $sender
 * @property mixed $data
 * @property bool $dropped
 *
 * @package AmqpEventsLib
 */
interface IMessage
{
}
