<?php
/**
 * IConsumer.php
 *
 * @author: chazer
 * @created: 18.08.15 16:19
 */

namespace AmqpEventsLib\Interfaces;

use Symfony\Component\EventDispatcher\EventDispatcherInterface;

interface IConsumer
{
    const ON_MESSAGE = 'consumer.onMessage';
    const ON_BEFORE_MESSAGE = 'consumer.onBeforeMessage';
    const ON_AFTER_MESSAGE = 'consumer.onAfterMessage';

    /**
     * @return string
     */
    public function getName();

    /**
     * @param mixed $queue
     */
    public function setQueueObject($queue);

    /**
     * @return mixed
     */
    public function getQueueObject();

    /**
     * @param mixed $consumer
     */
    public function setConsumerObject($consumer);

    /**
     * @return mixed
     */
    public function getConsumerObject();

    /**
     * @param IMessage $message
     */
    public function processMessage(IMessage $message);

    /**
     * @return EventDispatcherInterface
     */
    public function getDispatcher();

    /**.
     * @param callable $callback
     * @param string|string[] $events
     */
    public function addMessageListener($callback, $events = null);

    /**
     * @param callable $callback
     * @param string|string[] $events
     * @return mixed
     */
    public function removeMessageListener($callback, $events = null);

    /**
     * @return int
     */
    public function getListenersCount();
}
