<?php
/**
 * PhpAmqpAdapter.php
 *
 * @author: chazer
 * @created: 13.08.15 0:24
 */

namespace AmqpEventsLib\Adapters;

use AmqpEventsLib\AmqpEventsLib;
use AmqpEventsLib\Exceptions\TimeoutException;
use AmqpEventsLib\Interfaces\IAmqpAdapter;
use AmqpEventsLib\Interfaces\IMessage;
use AmqpEventsLib\Message;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AbstractConnection;
use PhpAmqpLib\Exception\AMQPProtocolChannelException;
use PhpAmqpLib\Exception\AMQPTimeoutException;
use PhpAmqpLib\Message\AMQPMessage;
use RuntimeException;

/**
 * Class PhpAmqpAdapter
 * @package AmqpEventsLib\adapters
 */
class PhpAmqpAdapter implements IAmqpAdapter
{
    /**
     * @var \PhpAmqpLib\Connection\AbstractConnection
     */
    private $connection;

    /**
     * @param AbstractConnection $amqp
     */
    public function __construct(AbstractConnection $amqp)
    {
        $this->connection = $amqp;
    }

    /**
     * @return AMQPChannel
     */
    public function openChannel()
    {
        return $this->connection->channel($this->connection->get_free_channel_id());
    }

    /**
     * @param AMQPChannel $channel
     */
    public function closeChannel($channel)
    {
        $this->assertChannel($channel);

        $channel->close();
    }

    /**
     * @param AMQPChannel $channel
     * @param array $params
     * @return PhpAmqpAdapter_QueueObject
     */
    protected function declareQueue(AMQPChannel $channel, $params)
    {
        $q = new PhpAmqpAdapter_QueueObject();
        $result = $channel->queue_declare(
            $params['name'],
            $params['isPassive'],
            $params['isDurable'],
            $params['isExclusive'],
            $params['autoDelete']
        );

        $q->identifier = array_shift($result);

        return $q;
    }

    /**
     * @param AMQPChannel $channel
     * @param array $params
     * @return PhpAmqpAdapter_ExchangeObject.
     */
    protected function declareExchange(AMQPChannel $channel, $params)
    {
        $ex = new PhpAmqpAdapter_ExchangeObject();

        $channel->exchange_declare(
            $params['name'],
            $params['type'],
            $params['isPassive'],
            $params['isDurable'],
            $params['autoDelete']
        );

        $ex->identifier = $params['name'];
        $ex->type = $params['type'];

        return $ex;
    }

    /**
     * @param AMQPChannel $channel
     * @return PhpAmqpAdapter_QueueObject
     */
    public function registerTempQueue($channel)
    {
        $this->assertChannel($channel);

        $qd = [
            //'isExclusive' => false,
            'isPassive' => false,
            'isDurable' => true,
            'autoDelete' => true,
            // ----
            'name' => 'listener_' . md5(microtime(true) . getmypid()),
            'isExclusive' => true,
        ];

        return $this->declareQueue($channel, $qd);
    }

    /**
     * @param AMQPChannel $channel
     * @param string $name
     * @return PhpAmqpAdapter_QueueObject
     */
    public function registerPersistentQueue($channel, $name)
    {
        $this->assertChannel($channel);

        $qDef = [
            'isExclusive' => false,
            'isPassive' => false,
            'isDurable' => true,
            //'autoDelete' => true,
            // ----
            'name' => $name,
            'autoDelete' => false,
        ];

        return $this->declareQueue($channel, $qDef);
    }


    /**
     * @param AMQPChannel $channel
     * @param PhpAmqpAdapter_QueueObject $queue
     */
    public function removeQueue($channel, $queue)
    {
        $this->assertChannel($channel);
        $this->assertQueue($queue);

        $channel->queue_delete($queue->identifier);
    }

    /**
     * @param AMQPChannel $channel
     * @param PhpAmqpAdapter_ExchangeObject $exchange
     * @param PhpAmqpAdapter_QueueObject $queue
     * @param string $routingKey
     * @throws \RuntimeException
     */
    public function bindQueue($channel, $exchange, $queue, $routingKey = null)
    {
        $this->assertChannel($channel);
        $this->assertExchange($exchange);
        $this->assertQueue($queue);

        $channel->queue_bind($queue->identifier, $exchange->identifier, $routingKey);
    }

    /**
     * @param AMQPChannel $channel
     * @param PhpAmqpAdapter_ExchangeObject $exchange
     * @param PhpAmqpAdapter_QueueObject $queue
     * @throws \RuntimeException
     */
    public function unbindQueue($channel, $exchange, $queue)
    {
        $this->assertChannel($channel);
        $this->assertExchange($exchange);
        $this->assertQueue($queue);

        $channel->queue_unbind($queue->identifier, $exchange->identifier);
    }


    /**
     * @param AMQPChannel $channel
     * @param PhpAmqpAdapter_QueueObject $queue
     * @param callable $callback
     * @param string $tag
     * @param bool $excludeLocal
     * @param bool $noAck
     * @param bool $isExclusive
     * @param bool $noWait
     * @return mixed consumer
     */
    public function consumeQueue($channel, $queue, $callback,
                                 $tag = '', $excludeLocal = false, $noAck = false,
                                 $isExclusive = true, $noWait = false)
    {
        $this->assertChannel($channel);
        $this->assertQueue($queue);

        return $channel->basic_consume($queue->identifier, $tag, $excludeLocal, $noAck, $isExclusive, $noWait,
            $callback);
    }

    /**
     * @param AMQPChannel $channel
     * @param mixed $consumer
     */
    public function cancelConsume($channel, $consumer, $nowait = false)
    {
        $channel->basic_cancel($consumer, $nowait);
    }

    /**
     * @param IMessage $message
     * @return AMQPMessage
     */
    public function packMessage(IMessage $message)
    {
        $options = [];
        isset($message->contentType) && $options['content_type'] = $message->contentType;
        unset ($message->contentType);

        $content = json_encode($message);

        return new AMQPMessage($content, array_merge([
            'delivery_mode' => 2,
        ], $options));
    }

    /**
     * @param AMQPMessage $message
     * @return IMessage
     */
    public function unpackMessage($message)
    {
        $this->assertMessage($message);

        $m = new Message();
        $decode = json_decode($message->body);
        foreach (get_object_vars($decode) as $name => $value) {
            $m->$name = $value;
        }
        return $m;
    }

    public function isExchangeExist($name)
    {
        $eDef = [
            'type' => 'direct',
            //'isPassive' => false,
            'isDurable' => true,
            'autoDelete' => true,
            // ----
            'name' => $name,
            'isPassive' => true,
        ];

        $tempChannel = $this->openChannel();
        $closeChannel = true;
        $exception = null;
        $exists = true;

        try {
            $this->declareExchange($tempChannel, $eDef);
        } catch (\Exception $e) {
            $exception = $e;
            if ($e instanceof AMQPProtocolChannelException) {
                $closeChannel = false;
                if ($e->amqp_reply_code === 404) { // Exchange not found
                    $exception = null;
                    $exists = false;
                }
            }
        }

        if ($closeChannel) {
            $this->closeChannel($tempChannel);
        }
        if ($exception) throw $exception;
        return $exists;
    }

    /**
     * @param AMQPChannel $channel
     * @param string $name
     * @param array $options exchange options
     * @return PhpAmqpAdapter_ExchangeObject
     * @throws \RuntimeException
     */
    public function createFanoutExchange($channel, $name, $options = [])
    {
        $this->assertChannel($channel);

        $eDef = [
            //'type' => 'direct',
            'isPassive' => false,
            'isDurable' => true,
            'autoDelete' => true,
            // ----
            'name' => $name,
            'type' => AmqpEventsLib::EX_TYPE_FANOUT,
        ];
        $params = array_merge($eDef, $options);

        return $this->declareExchange($channel, $params);
    }

    /**
     * @param AMQPChannel $channel
     * @param PhpAmqpAdapter_ExchangeObject $exchange
     * @throws \RuntimeException
     */
    public function removeExchange($channel, $exchange)
    {
        $this->assertChannel($channel);
        $this->assertExchange($exchange);

        $channel->exchange_delete($exchange->identifier);
    }

    /**
     * @param AMQPChannel $channel
     * @param PhpAmqpAdapter_ExchangeObject $exchange
     * @param string $routingKey
     * @param AMQPMessage $message
     * @param array $options
     * @throws \RuntimeException
     */
    public function sendMessage($channel, $exchange, $routingKey, $message, $options = [])
    {
        $this->assertChannel($channel);
        $this->assertExchange($exchange);
        $this->assertMessage($message);

        $channel->basic_publish($message, $exchange->identifier, $routingKey);
    }

    /**
     * @param AMQPChannel $channel
     * @param int $timeout
     * @throws \RuntimeException
     * @throws \AmqpEventsLib\Exceptions\TimeoutException
     */
    public function waitChannel($channel, $timeout)
    {
        $this->assertChannel($channel);

        try {
            $channel->wait(null, true, $timeout);
        } catch (AMQPTimeoutException $e) {
            throw new TimeoutException();
        }
    }

    /**
     * @param AMQPChannel $channel
     * @return int
     * @throws \RuntimeException
     */
    public function getConsumersCount($channel)
    {
        $this->assertChannel($channel);

        return count($channel->callbacks);
    }

    /**
     * @param $exchange
     * @throws \RuntimeException
     */
    private function assertExchange($exchange)
    {
        if (!$exchange instanceof PhpAmqpAdapter_ExchangeObject)
            throw new RuntimeException('Unsupported argument given on $exchange');
    }

    /**
     * @param $queue
     * @throws \RuntimeException
     */
    private function assertQueue($queue)
    {
        if (!$queue instanceof PhpAmqpAdapter_QueueObject)
            throw new RuntimeException('Unsupported argument given on $queue');
    }

    /**
     * @param $channel
     * @throws \RuntimeException
     */
    private function assertChannel($channel)
    {
        if (!$channel instanceof AMQPChannel)
            throw new RuntimeException('Unsupported argument given on $channel');
    }

    /**
     * @param $message
     * @throws \RuntimeException
     */
    private function assertMessage($message)
    {
        if (!$message instanceof AMQPMessage)
            throw new RuntimeException('Unsupported argument given on $message');
    }

    /**
     * @param AMQPMessage $message
     */
    public function ackMessage($message)
    {
        $this->assertMessage($message);

        $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag'], false);
    }

    /**
     * @param AMQPMessage $message
     * @param bool $requeue
     */
    public function nackMessage($message, $requeue = false)
    {
        $this->assertMessage($message);

        $message->delivery_info['channel']->basic_nack($message->delivery_info['delivery_tag'], false, $requeue);
    }

    /**
     * @param AMQPMessage $message
     * @param bool $requeue
     */
    public function rejectMessage($message, $requeue = true)
    {
        $this->assertMessage($message);

        $message->delivery_info['channel']->basic_reject($message->delivery_info['delivery_tag'], $requeue);
    }

    /**
     * @param AMQPMessage $message
     * @param bool $requeue
     */
    public function recoverMessage($message, $requeue = false)
    {
        $this->assertMessage($message);

        $message->delivery_info['channel']->basic_recover($requeue);
    }

    /**
     * @param PhpAmqpAdapter_ExchangeObject $exchange
     */
    public function getExchangeType($exchange)
    {
        $this->assertExchange($exchange);
        return $exchange->type;
    }
}

class PhpAmqpAdapter_QueueObject
{
    /**
     * @var mixed
     */
    public $identifier;
}

class PhpAmqpAdapter_ExchangeObject
{
    /**
     * @var mixed
     */
    public $identifier;

    /**
     * @var string
     */
    public $type;
}
