<?php
/**
 * ListenersBag.php
 *
 * @author: chazer
 * @created: 14.08.15 23:17
 */

namespace AmqpEventsLib;

class ObjectsList implements \Iterator, \Countable
{
    private $array = [];

    private $valid;

    public function indexOf($object)
    {
        foreach ($this->array as $key => $item) {
            if ($this->compare($item[0], $object)) {
                return $key;
            }
        }
        return -1;
    }

    public function hasItem($id)
    {
        return isset($this->array[$id]);
    }

    public function addItem($object, $data = null)
    {
        array_push($this->array, [
            0 => $object,
            1 => $data,
        ]);
        end($this->array);
        return key($this->array);
    }

    public function getItem($index)
    {
        return $this->array[$index][0];
    }

    public function setItemData($index, $data)
    {
        if ($index < 0 || !isset($this->array[$index])) {
            throw new \Exception('Item not found');
        }
        $this->array[$index][1] = $data;
    }

    public function getItemData($index)
    {
        return $this->array[$index][1];
    }

    public function removeItem($index)
    {
        unset($this->array[$index]);
    }

    public function current()
    {
        $item = current($this->array);
        return $item[0];
    }

    public function next()
    {
        next($this->array);
        $this->valid = key($this->array) !== null;
    }

    public function key()
    {
        return key($this->array);
    }

    public function valid()
    {
        return $this->valid;
    }

    public function rewind()
    {
        reset($this->array);
        $this->valid = !empty($this->array);
    }

    public function count()
    {
        return count($this->array);
    }

    /**
     * @param mixed $a
     * @param mixed $b
     * @return bool
     */
    protected function compare($a, $b)
    {
        if (gettype($a) !== gettype($b))
            return false;
        if (is_object($a))
            return $a === $b;
        if ($a === $b) {
            return true;
        }
        if (is_array($a)) {
            if (count($a) !== count($b))
                return false;
            $ka = array_keys($a);
            $kb = array_keys($b);
            if (count(array_diff(array_merge($ka, $kb), array_intersect($ka, $kb))) !== 0)
                return false;
            foreach ($a as $k => $va) {
                if (false === $this->compare($va, $b[$k]))
                    return false;
            }
        }
        return true;
    }
}
