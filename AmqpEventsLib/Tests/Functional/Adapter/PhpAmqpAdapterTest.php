<?php
/**
 * PhpAmqpAdapterTest.php
 *
 * @author: chazer
 * @created: 14.08.15 1:29
 */

namespace AmqpEventsLib\Tests\Functional\Adapter;

use AmqpEventsLib\Adapters\PhpAmqpAdapter;
use AmqpEventsLib\Interfaces\IAmqpAdapter;
use AmqpEventsLib\Interfaces\IMessage;
use AmqpEventsLib\Message;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Exception\AMQPProtocolChannelException;

class PhpAmqpAdapterTest extends \PHPUnit_Framework_TestCase
{
    /** @var AMQPStreamConnection */
    protected $conn;

    /** @var IAmqpAdapter */
    protected $adapter;

    public function setUp()
    {
        $this->conn = new AMQPStreamConnection(HOST, PORT, USER, PASS, VHOST);
        $this->adapter = new PhpAmqpAdapter($this->conn);
    }

    protected function genRandomName()
    {
        return substr(base64_encode(sha1(rand(0, PHP_INT_MAX) . microtime(true))), 0, 16);
    }

    public function testOpenCloseChannel()
    {
        $channel = $this->adapter->openChannel();
        $this->adapter->closeChannel($channel);
    }

    public function testCreateRemoveExchange()
    {
        $exchangeName = $this->genRandomName();
        $result = $this->adapter->isExchangeExist($exchangeName);
        $this->assertFalse($result);

        $channel = $this->adapter->openChannel();
        $exchange = $this->adapter->createFanoutExchange($channel, $exchangeName);
        $result = $this->adapter->isExchangeExist($exchangeName);
        $this->assertTrue($result);

        $this->adapter->removeExchange($channel, $exchange);

        $result = $this->adapter->isExchangeExist($exchangeName);
        $this->assertFalse($result);
    }

    public function testPersistentQueue()
    {
        /** @var IMessage[] $messages */
        $messages = [];

        $message = new Message();
        $message->data = $this->genRandomName();

        $channel = $this->adapter->openChannel();
        $exchange = $this->adapter->createFanoutExchange($channel, $exchangeName = $this->genRandomName());
        $queue = $this->adapter->registerPersistentQueue($channel, $queueName = $this->genRandomName());
        $this->adapter->bindQueue($channel, $exchange, $queue);
        $this->adapter->sendMessage($channel, $exchange, 'test', $this->adapter->packMessage($message));
        $this->adapter->closeChannel($channel);
        $this->conn->reconnect();

        $channel = $this->adapter->openChannel();
        $this->adapter->consumeQueue($channel, $queue,
            function ($m) use (&$messages) {
                array_push($messages, $this->adapter->unpackMessage($m));
            });

        $this->adapter->waitChannel($channel, 1);
        $this->adapter->removeQueue($channel, $queue);
        $this->adapter->removeExchange($channel, $exchange);
        $this->adapter->closeChannel($channel);

        $this->assertCount(1, $messages);
        $this->assertEquals($message->data, $messages[0]->data);
    }

    public function testTempQueue()
    {
        $exception = null;

        $message = new Message();
        $message->data = $this->genRandomName();

        $channel = $this->adapter->openChannel();
        $exchange = $this->adapter->createFanoutExchange($channel, $exchangeName = $this->genRandomName());
        $queue = $this->adapter->registerTempQueue($channel, $queueName = $this->genRandomName());
        $this->adapter->bindQueue($channel, $exchange, $queue);
        $this->adapter->sendMessage($channel, $exchange, 'test', $this->adapter->packMessage($message));
        $this->adapter->closeChannel($channel);
        $this->conn->reconnect();

        $channel = $this->adapter->openChannel();
        try {
            $this->adapter->consumeQueue($channel, $queue, function ($m) {
            });
        } catch (AMQPProtocolChannelException $e) {
            $exception = $e;
            // old channel was died
            $channel = $this->adapter->openChannel();
        }
        //$this->adapter->removeQueue($channel, $queue);
        $this->adapter->removeExchange($channel, $exchange);
        $this->adapter->closeChannel($channel);

        $this->setExpectedException('PhpAmqpLib\Exception\AMQPProtocolChannelException', '', 404);

        if ($exception)
            throw $exception;
    }

    public function testPackUnpack()
    {
        $message = new Message();
        $message->sender = 'sender';
        $message->data = 'data';
        $packed = $this->adapter->packMessage($message);
        $unpacked = $this->adapter->unpackMessage($packed);
        $this->assertEquals('sender', $unpacked->sender);
        $this->assertEquals('data', $unpacked->data);
    }

    public function test()
    {
    }
}
