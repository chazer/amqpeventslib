<?php
/**
 * SwapListenersBugTest.php
 *
 * @author: chazer
 * @created: 17.08.15 13:43
 */

namespace AmqpEventsLib\Tests\Functional\Bugs;

use AmqpEventsLib\Adapters\PhpAmqpAdapter;
use AmqpEventsLib\AmqpEventsLib;
use AmqpEventsLib\ConsumerSubscriber;
use AmqpEventsLib\Events\Internal\MessageEvent;
use AmqpEventsLib\Interfaces\IAmqpAdapter;
use AmqpEventsLib\Interfaces\IConsumer;
use AmqpEventsLib\Interfaces\IMessage;
use PhpAmqpLib\Connection\AMQPStreamConnection;

class SwapListenersBugTest extends \PHPUnit_Framework_TestCase
{
    /** @var AMQPStreamConnection */
    protected $conn;

    /** @var IAmqpAdapter */
    protected $adapter;

    /** @var AmqpEventsLib */
    protected $lib;

    public function setUp()
    {
        $this->conn = new AMQPStreamConnection(HOST, PORT, USER, PASS, VHOST);
        $this->adapter = new PhpAmqpAdapter($this->conn);

        $this->lib = new AmqpEventsLib();
        $this->lib->setAdapter(new PhpAmqpAdapter(new AMQPStreamConnection(HOST, PORT, USER, PASS, VHOST)));
        $this->lib->exchange = 'events';
        $this->lib->defaultSenderName = 'system';

        $this->lib->initExchange();
    }

    private function createListener($name)
    {
        /** @var callback $handler */
        $handler = function (MessageEvent $event) use (&$handler, $name) {
            $this->lib->sendMessage('hello', 'Hello from ' . $name, ['sender' => $name]);
            $this->lib->removeListener(AmqpEventsLib::ON_MESSAGE, $handler);
        };
        $this->lib->addListener(AmqpEventsLib::ON_MESSAGE, $handler);
    }

    private function createSubscriber($name)
    {
        /** @var ConsumerSubscriber $handler */
        $handler = new ConsumerSubscriber(
            function (IMessage $message) use (&$handler, $name) {
                $this->lib->sendMessage('hello', 'Hello from ' . $name, ['sender' => $name]);
                $this->lib->removeSubscriber($handler);
            }, ['intro']);
        $this->lib->addSubscriber($handler);
    }

    private function createConsumer($name)
    {
        $consumerName = $name . '_consumer';

        /** @var IConsumer $consumer */
        $consumer = $this->lib->createConsumer($consumerName);
        $this->lib->addConsumer($consumer);

        /** @var callback $handler */
        $handler = function (IMessage $message, IConsumer $consumer) use (&$handler, $name) {
            $this->lib->sendMessage('hello', 'Hello from ' . $name, ['sender' => $name]);
            $consumer->removeMessageListener($handler);
            $this->lib->removeConsumer($consumer, true);
        };
        $consumer->addMessageListener($handler);
    }

    private function createLogger(&$array)
    {
        $this->lib->addListener(AmqpEventsLib::ON_MESSAGE,
            function (MessageEvent $event) use (&$array) {
                $array[] = $event->message->data;
            });
    }

    /**
     * @dataProvider dataProvider
     */
    public function testMain($data)
    {
        $received = [];
        $this->createLogger($received);

        foreach ($data as $name => $type) {
            switch ($type) {
                case 'subscriber':
                    $this->createSubscriber($name);
                    break;
                case 'consumer':
                    $this->createConsumer($name);
                    break;
                case 'listener':
                    $this->createListener($name);
                    break;
            }
        }

        $this->lib->sendMessage('intro', 'Say');

        $this->lib->run(1, function () {
            $this->lib->stop();
        });

        foreach ($data as $name => $type) {
            $this->assertContains('Hello from ' . $name, $received);
        }
    }

    public function dataProvider()
    {
        return [
            [[
                'Mike' => 'subscriber',
                'John' => 'subscriber',
                'Sam' => 'subscriber',
            ]],
            [[
                'Mike' => 'subscriber',
                'Sam' => 'subscriber',
                'John' => 'subscriber',
            ]],

            [[
                'Mike' => 'consumer',
                'John' => 'consumer',
                'Sam' => 'consumer',
            ]],
            [[
                'Mike' => 'consumer',
                'Sam' => 'consumer',
                'John' => 'consumer',
            ]],

            [[
                'Mike' => 'listener',
                'John' => 'listener',
                'Sam' => 'listener',
            ]],
            [[
                'Mike' => 'listener',
                'Sam' => 'listener',
                'John' => 'listener',
            ]],

            [[
                'Mike' => 'subscriber',
                'John' => 'consumer',
                'Sam' => 'listener',
            ]],
            [[
                'Mike' => 'subscriber',
                'Sam' => 'listener',
                'John' => 'consumer',
            ]],

            [[
                'Mike' => 'consumer',
                'John' => 'subscriber',
                'Sam' => 'listener',
            ]],
            [[
                'Mike' => 'consumer',
                'Sam' => 'listener',
                'John' => 'subscriber',
            ]],

            [[
                'Mike' => 'listener',
                'John' => 'consumer',
                'Sam' => 'listener',
            ]],
            [[
                'Mike' => 'listener',
                'Sam' => 'listener',
                'John' => 'consumer',
            ]],
        ];
    }
}
