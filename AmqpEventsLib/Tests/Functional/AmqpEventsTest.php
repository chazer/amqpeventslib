<?php
/**
 * AmqpEventsTest.php
 *
 * @author: chazer
 * @created: 19.08.15 12:37
 */

namespace AmqpEventsLib\Tests\Functional;

use AmqpEventsLib\Adapters\PhpAmqpAdapter;
use AmqpEventsLib\AmqpEventsLib;
use AmqpEventsLib\ConsumerSubscriber;
use AmqpEventsLib\Events\Internal\MessageEvent;
use AmqpEventsLib\Interfaces\IAmqpAdapter;
use AmqpEventsLib\Interfaces\IConsumer;
use AmqpEventsLib\Interfaces\IMessage;
use PhpAmqpLib\Connection\AMQPStreamConnection;

class AmqpEventsTest extends \PHPUnit_Framework_TestCase
{
    /** @var AMQPStreamConnection */
    protected $conn;

    /** @var IAmqpAdapter */
    protected $adapter;

    /** @var AmqpEventsLib */
    protected $lib;

    public function setUp()
    {
        $this->conn = new AMQPStreamConnection(HOST, PORT, USER, PASS, VHOST);
        $this->adapter = new PhpAmqpAdapter($this->conn);

        $this->lib = new AmqpEventsLib();
        $this->lib->setAdapter(new PhpAmqpAdapter(new AMQPStreamConnection(HOST, PORT, USER, PASS, VHOST)));
        $this->lib->exchange = 'events';
        $this->lib->defaultSenderName = 'system';

        $this->lib->initExchange();
    }

    private function createListener($name)
    {
        /** @var callback $handler */
        $handler = function (MessageEvent $event) use (&$handler, $name) {
            if ($event->consumer->getName())
                return;
            $this->lib->sendMessage('hello', 'Hello from ' . $name, ['sender' => $name]);
            $this->lib->removeListener(AmqpEventsLib::ON_MESSAGE, $handler);
        };
        $this->lib->addListener(AmqpEventsLib::ON_MESSAGE, $handler);
    }

    private function createSubscriber($name)
    {
        /** @var ConsumerSubscriber $handler */
        $handler = new ConsumerSubscriber(
            function (IMessage $message, IConsumer $consumer) use (&$handler, $name) {
                $this->lib->sendMessage('hello', 'Hello from ' . $name, ['sender' => $name]);
                $consumer->getDispatcher()->removeSubscriber($handler);
            }, ['intro']);
        $this->lib->getConsumer()->getDispatcher()->addSubscriber($handler);
    }

    private function createConsumer($name)
    {
        $consumerName = $name . '_consumer';

        /** @var IConsumer $consumer */
        $consumer = $this->lib->createConsumer($consumerName);
        $this->lib->addConsumer($consumer);
        $handler = function (IMessage $message) use (&$consumer, &$handler, $name) {
            $this->lib->sendMessage('hello', 'Hello from ' . $name, ['sender' => $name]);
            $consumer->removeMessageListener($handler);
            $this->lib->removeConsumer($consumer, true);
        };
        $consumer->addMessageListener($handler);
    }

    private function createLogger(&$array)
    {
        $this->lib->addListener(AmqpEventsLib::ON_MESSAGE,
            function (MessageEvent $event) use (&$array) {
                $array[] = $event->message->data;
            });
    }

    public function testBasicConsumer()
    {
        $consumerName = 'temp_consumer';
        $data = null;

        /** @var IConsumer $consumer */
        $consumer = $this->lib->createConsumer($consumerName);
        $handler = function (IMessage $message) use (&$consumer, &$handler, &$data) {
            $data = $message->data;
            $consumer->removeMessageListener($handler);
            $this->lib->removeConsumer($consumer, true);
        };
        $this->lib->addConsumer($consumer);
        $consumer->addMessageListener($handler);

        $this->lib->sendMessage('intro', 'Say');
        $this->lib->run(1, function () {
            $this->lib->stop();
        });

        $this->assertEquals('Say', $data);
    }

    /**
     * @dataProvider dataProvider
     */
    public function testCount($data)
    {
        $received = [];
        $this->createLogger($received);

        $listeners = 1;
        $subscribers = 0;
        $consumers = 0;
        foreach ($data as $name => $type) {
            switch ($type) {
                case 'subscriber':
                    $this->createSubscriber($name);
                    $subscribers++;
                    break;
                case 'consumer':
                    $this->createConsumer($name);
                    $consumers += 2;
                    break;
                case 'listener':
                    $this->createListener($name);
                    $listeners++;
                    break;
            }
        }
        $expectedCount = $listeners + $subscribers + $consumers;

        $this->lib->sendMessage('intro', 'Say');

        $this->lib->run(1, function () {
            $this->lib->stop();
        });

        $this->assertCount($expectedCount, $received);
    }

    public function dataProvider()
    {
        return [
            [[
                'Mike' => 'consumer',
                'John' => 'subscriber',
                'Sam' => 'listener',
            ]],
            [[
                'Mike' => 'listener',
                'John' => 'consumer',
                'Sam' => 'listener',
            ]],
            [[
                'Mike' => 'listener',
                'Sam' => 'subscriber',
                'John' => 'consumer',
                'Bill' => 'consumer',
            ]],
            [[
                'Mike' => 'listener',
                'Sam' => 'subscriber',
                'John' => 'consumer',
                'Bill' => 'consumer',
                'Fred' => 'consumer',
            ]],
            [[
                'Mike' => 'listener',
                'Sam' => 'subscriber',
                'Adam' => 'subscriber',
                'John' => 'consumer',
                'Bill' => 'consumer',
                'Fred' => 'consumer',
            ]],
        ];
    }
}
