<?php
/**
 * ObjectsListtest.php
 *
 * @author: chazer
 * @created: 19.08.15 1:13
 */

namespace AmqpEventsLib\Tests\Unit;

use AmqpEventsLib\ObjectsList;

class ObjectsListTest extends \PHPUnit_Framework_TestCase
{
    /** @var ObjectsList */
    protected $list;

    public function setUp()
    {
        $this->list = new ObjectsList();
    }

    public function testAdd()
    {
        $obj = new \stdClass();
        $cb = function () {
        };

        $this->list->addItem(0);
        $this->assertEquals(0, $this->list->indexOf(0));

        $this->list->addItem(null);
        $this->assertEquals(1, $this->list->indexOf(null));

        $this->list->addItem(false);
        $this->assertEquals(2, $this->list->indexOf(false));

        $this->list->addItem('0');
        $this->assertEquals(3, $this->list->indexOf('0'));

        $this->list->addItem($obj);
        $this->assertEquals(4, $this->list->indexOf($obj));

        $this->list->addItem($cb);
        $this->assertEquals(5, $this->list->indexOf($cb));

        $this->list->addItem([]);
        $this->assertEquals(6, $this->list->indexOf([]));

        $this->list->addItem([null]);
        $this->assertEquals(7, $this->list->indexOf([null]));

        $this->list->addItem([false]);
        $this->assertEquals(8, $this->list->indexOf([false]));

        $this->list->addItem([['0']]);
        $this->assertEquals(9, $this->list->indexOf([['0']]));

        $this->list->addItem([[]]);
        $this->assertEquals(10, $this->list->indexOf([[]]));

        $this->list->addItem([1, 2]);
        $this->assertEquals(11, $this->list->indexOf([1, 2]));

        $this->list->addItem(['a' => 1, 'b' => 2]);
        $this->assertEquals(12, $this->list->indexOf(['b' => 2, 'a' => 1]));

        $this->list->addItem([null, $obj]);
        $this->assertEquals(13, $this->list->indexOf([null, $obj]));

        $this->list->addItem([$obj, 'string']);
        $this->assertEquals(14, $this->list->indexOf([$obj, 'string']));

        $this->list->addItem([null, $cb]);
        $this->assertEquals(15, $this->list->indexOf([null, $cb]));
    }

    public function testRemove()
    {
        $this->list->addItem('a');
        $this->list->addItem('b');
        $this->list->addItem('c');

        $this->assertCount(3, $this->list);

        $this->list->removeItem(1);

        $this->assertCount(2, $this->list);
        $this->assertTrue($this->list->hasItem(0));
        $this->assertFalse($this->list->hasItem(1));
        $this->assertTrue($this->list->hasItem(2));
    }
}
