<?php
/**
 * ConsumerTest.php
 *
 * @author: chazer
 * @created: 05.09.15 18:25
 */

namespace AmqpEventsLib\Tests\Unit;

use AmqpEventsLib\Consumer;
use AmqpEventsLib\Interfaces\IConsumer;
use AmqpEventsLib\Interfaces\IMessage;
use AmqpEventsLib\Message;

class ConsumerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @param string $name
     * @return IConsumer
     */
    protected function initConsumerInstance($name = null)
    {
        return new Consumer($name);
    }

    public function testConsumerName()
    {
        $name = 'testConsumerName';
        $consumer = $this->initConsumerInstance($name);
        $this->assertEquals($name, $consumer->getName());
    }

    public function testAddRemoveListeners_ListenersCount()
    {
        $l = function () {
        };
        $consumer = $this->initConsumerInstance();

        $count = $consumer->getListenersCount();
        $this->assertEquals(0, $count);

        $consumer->addMessageListener($l, null);
        $this->assertGreaterThan($count, $count = $consumer->getListenersCount());

        $consumer->removeMessageListener($l, null);
        $this->assertLessThan($count, $count = $consumer->getListenersCount());

        $consumer->addMessageListener($l, 'test.a');
        $this->assertGreaterThan($count, $count = $consumer->getListenersCount());

        $consumer->addMessageListener($l, 'test.b');
        $this->assertEquals($count, $count = $consumer->getListenersCount());

        $consumer->addMessageListener($l, 'test.c');
        $this->assertEquals($count, $count = $consumer->getListenersCount());

        $consumer->removeMessageListener($l, 'test.b');
        $this->assertEquals($count, $count = $consumer->getListenersCount());

        $consumer->removeMessageListener($l, ['test.a', 'test.c']);
        $this->assertLessThan($count, $count = $consumer->getListenersCount());

        $this->assertEquals(0, $count);

        $consumer->addMessageListener($l, 'test.a');
        $this->assertGreaterThan($count, $count = $consumer->getListenersCount());

        $consumer->addMessageListener($l, 'test.b');
        $this->assertEquals($count, $count = $consumer->getListenersCount());

        $consumer->removeMessageListener($l, null);
        $this->assertLessThan($count, $count = $consumer->getListenersCount());

        $this->assertEquals(0, $count);
    }

    /**
     * @param array $lr
     * @param mixed $events
     * @param IMessage $message
     *
     * @dataProvider listenersDataProvider
     */
    public function testAddRemoveListeners_ProcessMessage($lr, $events, IMessage $message)
    {
        /**
         * @var callable $listener
         * @var callable $resolver
         */
        list($listener, $resolver) = $lr;

        // Test A
        $consumer = $this->initConsumerInstance();
        $consumer->addMessageListener($listener, $events);
        $consumer->processMessage($message);
        $consumer->removeMessageListener($listener, $events);
        $this->assertTrue($resolver() === $message);
    }

    public function listenersDataProvider()
    {
        $createPair = function () {
            $msg = null;
            $listener = function (IMessage $message) use (&$msg) {
                $msg = $message;
            };
            $resolver = function () use (&$msg) {
                return $msg;
            };
            return [$listener, $resolver];
        };
        // TODO: test with object callback

        $out = [];

        $m = new Message();
        $m->event = 'event';
        $out[] = [$createPair(), null, $m];

        $m = new Message();
        $m->event = 'event';
        $out[] = [$createPair(), 'event', $m];

        $m = new Message();
        $m->event = 'event';
        $out[] = [$createPair(), ['event'], $m];

        $m = new Message();
        $m->event = 'event';
        $out[] = [$createPair(), ['event', 'event.x'], $m];

        return $out;
    }
}
