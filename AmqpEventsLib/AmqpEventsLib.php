<?php
/**
 * AmqpEventsLib.php
 *
 * @author: chazer
 * @created: 12.08.15 23:56
 */

namespace AmqpEventsLib;

use AmqpEventsLib\Events\Internal\MessageEvent;
use AmqpEventsLib\Exceptions\ObjectReturnException;
use AmqpEventsLib\Exceptions\TimeoutException;
use AmqpEventsLib\Interfaces\IAmqpAdapter;
use AmqpEventsLib\Interfaces\IConsumer;
use AmqpEventsLib\Interfaces\IMessage;
use Exception;
use InvalidArgumentException;
use RuntimeException;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class AmqpEventsLib implements EventDispatcherInterface
{
    const ON_CHANNEL_TIMEOUT = 'AmqpEventsLib\AmqpEvents.onChannelTimeout';
    const ON_BEFORE_MESSAGE = 'AmqpEventsLib\AmqpEvents.onBeforeMessage';
    const ON_AFTER_MESSAGE = 'AmqpEventsLib\AmqpEvents.onAfterMessage';
    const ON_MESSAGE = 'AmqpEventsLib\AmqpEvents.onMessage';
    const ON_WAIT = 'AmqpEventsLib\AmqpEvents.onWait';

    const EX_TYPE_FANOUT = 'fanout';
    const EX_TYPE_TOPIC = 'topic';

    /**
     * @var string
     */
    public $exchange = 'events';

    /**
     * @var array <pre>Supported options:
     *     'name' - exchange name
     *     'type' - exchange type (fanout, topic, direct)
     *     'isPassive' - dry run
     *     'isDurable' - restore messages after server restart
     *     'autoDelete' - have delete exchange after all queues have been unbound
     * </pre>
     */
    public $exchangeOptions = [];

    /**
     * @var mixed
     */
    private $_exchange;

    /**
     * @var IAmqpAdapter
     */
    private $_amqpAdapter;

    private $isWorking = false;
    private $stopLoop;

    private $mainChannel;
    private $publishChannel;

    private $consumers;
    private $mainConsumer;

    /** @var IConsumer[] */
    private $consumersList = [];

    private $proxyDispatcher;

    /**
     * @var string
     */
    public $defaultSenderName;

    public function __destruct()
    {
        $this->terminate();
    }

    protected function terminate()
    {
        $list = $this->getConsumersList();
        $callbacks = [];
        foreach ($list as $index => $listener)
            array_push($callbacks, $list->getItem($index));
        foreach ($callbacks as $listener)
            $this->removeConsumer($listener);
        unset($callbacks);

        if (isset($this->_amqpAdapter)) {
            if ($this->mainConsumer) {
                $this->_amqpAdapter->cancelConsume($this->mainChannel, $this->mainConsumer);
                $this->mainConsumer = null;
            }
            if ($this->publishChannel) {
                $this->_amqpAdapter->closeChannel($this->publishChannel);
                $this->publishChannel = null;
            }
            if ($this->mainChannel) {
                $this->_amqpAdapter->closeChannel($this->mainChannel);
                $this->mainChannel = null;
            }
        }
    }

    /**
     * @return IAmqpAdapter
     * @throws \RuntimeException
     */
    public function getAdapter()
    {
        if (!isset($this->_amqpAdapter)) {
            throw new RuntimeException('AmqpAdapter not defined');
        }
        return $this->_amqpAdapter;
    }

    /**
     * @param IAmqpAdapter $adapter
     * @return $this
     */
    public function setAdapter(IAmqpAdapter $adapter)
    {
        $this->_amqpAdapter = $adapter;
        return $this;
    }

    /**
     * @param mixed $channel
     * @return mixed
     */
    protected function registerTempQueue($channel)
    {
        if (isset($this->tempQueue)) {
            return $this->tempQueue;
        }
        $queue = $this->getAdapter()->registerTempQueue($channel);
        // Note: Call getExchange before getExchangeType!!!
        $exchange = $this->getExchange();
        $routingKey = null;
        if ($this->getExchangeType() === self::EX_TYPE_TOPIC) {
            $routingKey = '#';
        }
        $this->getAdapter()->bindQueue($channel, $exchange, $queue, $routingKey);
        return $this->tempQueue = $queue;
    }

    /**
     * @param mixed $channel
     * @param string $name
     * @return mixed
     */
    protected function registerPersistentQueue($channel, $name)
    {
        $queue = $this->getAdapter()->registerPersistentQueue($channel, $name);
        // Note: Call getExchange before getExchangeType!!!
        $exchange = $this->getExchange();
        $routingKey = null;
        if ($this->getExchangeType() === self::EX_TYPE_TOPIC) {
            $routingKey = '#';
        }
        $this->getAdapter()->bindQueue($channel, $exchange, $queue, $routingKey);
        return $queue;
    }

    /**
     * @param mixed $message
     */
    protected function ackMessage($message)
    {
        $this->getAdapter()->ackMessage($message);
    }

    /**
     * @param mixed $message
     * @param bool $requeue
     */
    protected function nackMessage($message, $requeue = false)
    {
        $this->getAdapter($message)->nackMessage($message, $requeue);
    }

    /**
     * @param mixed $message
     * @param bool $requeue
     */
    protected function rejectMessage($message, $requeue = true)
    {
        $this->getAdapter()->rejectMessage($message, $requeue);
    }

    /**
     * @return mixed
     */
    public function openChannel()
    {
        return $this->getAdapter()->openChannel();
    }

    /**
     * @param mixed $channel
     */
    public function closeChannel($channel)
    {
        $this->getAdapter()->closeChannel($channel);
    }

    /**
     * Channel for configure and receive
     *
     * @return mixed
     */
    protected function getMainChannel()
    {
        return $this->mainChannel ?: $this->mainChannel = $this->openChannel();
    }

    /**
     * Channel only for send messages
     *
     * @return mixed
     */
    protected function getPublishChannel()
    {
        return $this->publishChannel ?: $this->publishChannel = $this->openChannel();
    }

    /**
     * @param string|IMessage $event
     * @param mixed $data
     * @param array $options
     */
    public function sendMessage($event, $data = null, $options = [])
    {
        $this->init();
        $m = ($event instanceof IMessage) ? $event : $m = $this->createMessage($event, $data, $options);
        $message = $this->getAdapter()->packMessage($m);
        $this->getAdapter()->sendMessage(
            $this->getPublishChannel(),
            $this->getExchange(),
            $m->event,
            $message
        );
    }

    public function stop()
    {
        $this->stopLoop = true;
    }

    public function hasExchange($name)
    {
        return $this->getAdapter()->isExchangeExist($name);
    }

    /**
     * @param bool $create
     * @return mixed
     * @throws Exception
     */
    public function getExchange($create = false)
    {
        if (isset($this->_exchange)) {
            return $this->_exchange;
        }

        if ($create === false) {
            $exists = $this->getAdapter()->isExchangeExist($this->exchange);
            if (!$exists) {
                throw new Exception("Exchange '{$this->exchange}' not found'");
            }
        }

        $ex = $this->getAdapter()->createFanoutExchange($this->getMainChannel(), $this->exchange, $this->exchangeOptions);

        return $this->_exchange = $ex;
    }

    public function initExchange()
    {
        if (!$this->hasExchange($this->exchange)) {
            $this->getExchange(true);
        }
    }

    public function hasConsumer($name)
    {
        return isset($this->consumersList[$name]);
    }

    /**
     * @param string $name
     * @return IConsumer
     */
    public function getConsumer($name = null)
    {
        if ($name === null) {
            if (!isset($this->consumersList[null])) {
                $consumer = $this->createConsumer(null);
                $this->registerConsumer($consumer);
                $this->consumersList[null] = $consumer;
            }
        }
        return $this->consumersList[$name];
    }

    /**
     * @param string $name
     * @param IConsumer $consumer
     * @throws \InvalidArgumentException
     * @throws \Exception
     */
    public function addConsumer(IConsumer $consumer)
    {
//        if ($name !== null && !is_string($name)) {
//            throw new InvalidArgumentException('String expected');
//        }
//        if (!($callback instanceof IConsumer) && !is_callable($callback)) {
//            throw new InvalidArgumentException('Callback or instance of IConsumer expected');
//        }
        $name = $consumer->getName();

        $list = $this->getConsumersList();
        if (0 <= ($index = $list->indexOf($consumer))) {
            // listener already exists
            throw new Exception('Consumer is already attached');
        }

        if ($this->hasConsumer($name)) {
            throw new Exception('Consumer is already attached');
        }

        $search = $consumer;
        $listener = null;
        $data = ['consumerName' => $name];

        /** @var IConsumer $c */
//        if (!($consumer instanceof IConsumer)) {
//            $data['messageListener'] = $consumer;
//            $consumer = $this->createConsumer($name);
//            $consumer->addMessageListener($data['messageListener']);
//        }

        $this->consumersList[$name] = $consumer;
        $list->addItem($search, $data);
        $this->registerConsumer($consumer);
    }

    /**
     * @param string $name
     * @return Consumer
     */
    public function createConsumer($name)
    {
        $consumer = new Consumer($name);
        return $consumer;
    }

    /**
     * @param string|IConsumer $consumer
     * @param bool $deleteQueue
     * @throws \Exception
     */
    public function removeConsumer($consumer, $deleteQueue = false)
    {
        if (is_string($consumer) && $this->hasConsumer($consumer)) {
            $consumer = $this->getConsumer($consumer);
        }
        $consumerName = $consumer->getName();
        $list = $this->getConsumersList();
        if (0 > ($index = $list->indexOf($consumer))) {
            throw new Exception('Consumer not found');
        }
//        $data = $list->getItemData($index);
//        $consumerName = isset($data['consumerName']) ? $data['consumerName'] : null;
//        $listener = isset($data['messageListener']) ? $data['messageListener'] : null;

//        $c = $this->getConsumer($consumerName);
//        $listener && $c->removeMessageListener($listener);

        unset($this->consumersList[$consumerName]);
        $list->removeItem($index);
        $this->unregisterConsumer($consumer, $deleteQueue);
    }

    /**
     * @return int
     */
    public function getListenersCount()
    {
        $count = 0;
        foreach ($this->getConsumers() as $consumer)
            $count += $consumer->getListenersCount();
        return $count;
    }

    protected function processAdapterMessage(IConsumer $consumer, $message)
    {
        $m = $this->getAdapter()->unpackMessage($message);
        $m->dropped = false;

        $event = new MessageEvent($m, $consumer);
        $this->dispatch(self::ON_BEFORE_MESSAGE, $event);
        $reject = $event->isPropagationStopped();

        $reject = $reject || !$consumer->processMessage($m);

        if (!$reject && !$m->dropped) {
            $event = new MessageEvent($m, $consumer);
            $this->dispatch(self::ON_MESSAGE, $event);
        }

        if ($reject) {
            //$this->nackMessage($message, true);
            $this->rejectMessage($message, !$m->dropped);
            return;
        }
        if ($m->dropped) {
            $this->nackMessage($message, false);
        } else {
            $this->ackMessage($message);
        }
        $event = new MessageEvent($m, $consumer);
        $this->dispatch(self::ON_AFTER_MESSAGE, $event);
    }

    protected function registerConsumer(IConsumer $consumer)
    {
        $channel = $this->getMainChannel();
        $queue = $consumer->getName() !== null
            ? $this->registerPersistentQueue($channel, $consumer->getName())
            : $this->registerTempQueue($channel);

        $isExclusive = false;
        $noWait = false;
        $consumerTag = $this->getAdapter()->consumeQueue($channel, $queue,
            function ($message) use ($consumer) {
                $this->processAdapterMessage($consumer, $message);
            },
            '', false, false, $isExclusive, $noWait);

        $consumer->setQueueObject($queue);
        $consumer->setConsumerObject($consumerTag);
        $this->addProxyListeners($consumer);
    }

    protected function unregisterConsumer(Consumer $consumer, $deleteQueue = false)
    {
        $this->getAdapter()->cancelConsume($this->getMainChannel(), $consumer->getConsumerObject(), false);
        if ($deleteQueue) {
            $this->removeQueue($consumer->getQueueObject());
        }
        $this->removeProxyListeners($consumer);
    }

    protected function removeQueue($queue)
    {
        $ch = $this->getMainChannel();
        $this->getAdapter()->unbindQueue($ch, $this->getExchange(), $queue);
        $this->getAdapter()->removeQueue($ch, $queue);
    }

    public function run($timeout = 1, $waitCallback = null)
    {
        $this->init();
        if ($this->isWorking) {
            throw new \RuntimeException('Already in work');
        }

        $channel = $this->getMainChannel();
        while (true) {
            if ($this->stopLoop)
                break;

            $this->dispatch(self::ON_WAIT, new Event());

            try {
                if ($this->getAdapter()->getConsumersCount($channel) > 0) {
                    $this->getAdapter()->waitChannel($channel, $timeout);
                }
            } catch (TimeoutException $e) {
                unset($e);
                $this->onChannelTimeout();
                $waitCallback && call_user_func($waitCallback);
            }
        }

        $this->isWorking = false;

        $this->terminate();
    }

    /**
     * @return IConsumer[]
     */
    public function getConsumers()
    {
        return array_values($this->consumersList);
    }

    protected function onChannelTimeout()
    {
        $event = new Event();
        foreach ($this->getConsumers() as $consumer) {
            $consumer->getDispatcher()->dispatch(self::ON_CHANNEL_TIMEOUT, $event);
        }
        $this->dispatch(self::ON_CHANNEL_TIMEOUT, $event = new Event());
    }

    /**
     * @return ObjectsList
     */
    protected function getConsumersList()
    {
        return $this->consumers ?: $this->consumers = new ObjectsList();
    }

    /**
     * Creates a message that can be sent
     *
     * @param string $event the event name
     * @param string $content the message text
     * @param array $options the message options
     *
     * @return IMessage the message instance
     */
    public function createMessage($event, $content = null, $options = [])
    {
        $m = new Message();
        $m->event = $event;
        $m->data = $content;
        $m->sender = $this->defaultSenderName;
        // TODO: sender, options
        isset($options['sender']) && $m->sender = $options['sender'];
        return $m;
    }

    /**
     * @return EventDispatcher
     */
    protected function getProxyDispatcher()
    {
        return $this->proxyDispatcher ?: $this->proxyDispatcher = new EventDispatcher();
    }

    public function dispatch($eventName, Event $event = null)
    {
        return $this->getProxyDispatcher()->dispatch($eventName, $event);
    }

    /**
     * Attach event listener for all consumers
     *
     * @param string $eventName
     * @param callable $listener
     * @param int $priority
     */
    public function addListener($eventName, $listener, $priority = 0)
    {
        foreach ($this->getConsumers() as $consumer) {
            $consumer->getDispatcher()->addListener($eventName, $listener, $priority);
        }
        $this->getProxyDispatcher()->addListener($eventName, $listener, $priority);
    }

    /**
     * Detach event listener from all consumers
     *
     * @param string $eventName
     * @param callable $listener
     */
    public function removeListener($eventName, $listener)
    {
        foreach ($this->getConsumers() as $consumer) {
            $consumer->getDispatcher()->removeListener($eventName, $listener);
        }
        $this->getProxyDispatcher()->removeListener($eventName, $listener);
    }

    /**
     * Get event listeners shared with consumers
     *
     * @param null $eventName
     * @return array
     */
    public function getListeners($eventName = null)
    {
        return $this->getProxyDispatcher()->getListeners($eventName);
    }

    /**
     * Check if event has registered listeners on shared listeners stack
     *
     * @param null $eventName
     * @return bool
     */
    public function hasListeners($eventName = null)
    {
        return $this->getProxyDispatcher()->hasListeners($eventName);
    }

    /**
     * Register event subscriber on all consumers
     *
     * @param EventSubscriberInterface $subscriber
     */
    public function addSubscriber(EventSubscriberInterface $subscriber)
    {
        foreach ($this->getConsumers() as $consumer) {
            $consumer->getDispatcher()->addSubscriber($subscriber);
        }
        $this->getProxyDispatcher()->addSubscriber($subscriber);
    }

    /**
     * Remove event subscriber from all consumers
     *
     * @param EventSubscriberInterface $subscriber
     */
    public function removeSubscriber(EventSubscriberInterface $subscriber)
    {
        foreach ($this->getConsumers() as $consumer) {
            $consumer->getDispatcher()->removeSubscriber($subscriber);
        }
        $this->getProxyDispatcher()->removeSubscriber($subscriber);
    }

    /**
     * @param callable $listener
     * @param string|array $events
     * @param string $consumerName
     */
    public function addMessageListener($listener, $events = null, $consumerName = null)
    {
        $this->getConsumer($consumerName)->addMessageListener($listener, $events);
    }

    /**
     * @param callable $listener
     * @param string|array $events
     * @param string|array $consumerName
     */
    public function removeMessageListener($listener, $events = null, $consumerName = null)
    {
        $this->getConsumer($consumerName)->removeMessageListener($listener, $events);
    }

    /**
     * Attach all shared listeners to consumer
     *
     * @param IConsumer $consumer
     */
    protected function addProxyListeners(IConsumer $consumer)
    {
        foreach ($this->getProxyDispatcher()->getListeners(null, true) as $eventName => $listenersStack) {
            foreach ($listenersStack as $priority => $listeners) {
                foreach ($listeners as $listener) {
                    $consumer->getDispatcher()->addListener($eventName, $listener, $priority);
                }
            }
        }
    }

    /**
     * Detach all shared listeners from consumer
     *
     * @param IConsumer $consumer
     */
    protected function removeProxyListeners(IConsumer $consumer)
    {
        foreach ($this->getProxyDispatcher()->getListeners(null) as $eventName => $listeners) {
            foreach ($listeners as $listener) {
                $consumer->getDispatcher()->removeListener($eventName, $listener);
            }
        }
    }

    protected function init()
    {
        $this->initExchange();
        $this->getConsumer(null);
    }

    /**
     * Get type of exchange (fanout,topic,direct)
     *
     * @return null|string
     */
    public function getExchangeType()
    {
        if (isset($this->_exchange)) {
            return $this->getAdapter()->getExchangeType($this->_exchange);
        }
        return null;
    }
}
